<?php


namespace Extradevs\SzamlazzHu\Tests\Client\Invoice;


use Extradevs\SzamlazzHu\Client\Client;
use Extradevs\SzamlazzHu\Invoice;

class TestCase extends \Extradevs\SzamlazzHu\Tests\Client\TestCase {

    /**
     * @param null $number
     * @param Client|null $client
     * @return Invoice
     */
    protected function getEmptyInvoice($number = null, Client $client = null)
    {
        $invoice = new Invoice();

        if ($client) {
            $invoice->setClient($client);
        }

        if ($number) {
            $invoice->invoiceNumber = $number;
        }

        return $invoice;
    }

}
