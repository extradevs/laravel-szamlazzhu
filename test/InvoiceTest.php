<?php


namespace Extradevs\SzamlazzHu\Tests;


use Extradevs\SzamlazzHu\Internal\Support\ItemHolder;
use Extradevs\SzamlazzHu\Invoice;

class InvoiceTest extends \Orchestra\Testbench\TestCase {


    public function test_it_is_item_holder()
    {
        $this->assertArrayHasKey(ItemHolder::class, class_uses_recursive(Invoice::class));
    }

}
