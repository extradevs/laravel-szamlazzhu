<?php


namespace Extradevs\SzamlazzHu\Tests;


use Extradevs\SzamlazzHu\Internal\Support\ItemHolder;
use Extradevs\SzamlazzHu\Internal\Support\PaymentHolder;
use Extradevs\SzamlazzHu\Receipt;

class ReceiptTest extends \Orchestra\Testbench\TestCase {


    public function test_it_is_item_holder()
    {
        $this->assertArrayHasKey(ItemHolder::class, class_uses(Receipt::class));
    }


    public function test_it_is_payment_holder()
    {
        $this->assertArrayHasKey(PaymentHolder::class, class_uses(Receipt::class));
    }


    public function test_it_has_item_total_alias()
    {
        $this->assertTrue(method_exists(new Receipt(), 'totalItems'));
    }


    public function test_it_has_payment_total_alias()
    {
        $this->assertTrue(method_exists(new Receipt(), 'totalPayments'));
    }

}
