<?php


namespace Extradevs\SzamlazzHu\Contracts;

/**
 * Interface ArrayableInvoiceItemCollection
 * @package Extradevs\SzamlazzHu\Contracts
 */
interface ArrayableItemCollection
{

    /**
     * @see ArrayableItem
     * @return ArrayableItem[]
     */
    function toItemCollectionArray();

}
