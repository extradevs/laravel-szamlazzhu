<?php


namespace Extradevs\SzamlazzHu\Client\ApiErrors;


use Exception;

/**
 * Class AuthorizationException
 * @package Extradevs\SzamlazzHu\Client\ApiErrors
 *
 * Abstract authorization exception
 */
abstract class AuthorizationException extends Exception
{

}
